import dlib
import cv2
import numpy as np
import os

## aligning
def get_angle_with_X(p1, p2):
    """get angle between X axis and line defined by two points

        Note:
            Do not include the `self` parameter in the ``Args`` section.

        Args:
            p1: dlib.Point
            p2: dlib.Point

        Returns:
           degs : angle in degrees
    """
    from math import atan2, degrees, pi

    (x1, y1) = p1.x, p1.y
    (x2, y2) = p2.x, p2.y

    dx = x2 - x1
    dy = y2 - y1
    rads = atan2(-dy, dx)
    rads %= 2 * pi
    degs = degrees(rads)
    return degs


def rotateImage(image, rotation_point, angle):
    """rotate ``image`` by ``angle`` around ``rotation_point``"""
    rot_mat = cv2.getRotationMatrix2D(rotation_point, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, None)  # , image.shape[:2])
    return result


def center_between_points(p1, p2):
    return np.array([(p1.x + p2.x) / 2, (p1.y + p2.y) / 2])


def transform_points(p, ang, s0, s1):
    """rotate and scale point around (0,0)

        Note:


        Args:
            p: dlib.Point
            ang: rotation angle in degrees
            s0: tuple, old image size
            s1: tuple, new image size

        Returns:
           (xx, yy) : ``numpy.ndarray`` new coordinates
    """
    x0 = p[0] - s0[1] / 2
    y0 = p[1] - s0[0] / 2
    ang_in_rad = ang * np.pi / 180
    xx = x0 * np.cos(ang_in_rad) - y0 * np.sin(ang_in_rad) + s1[1] / 2
    yy = x0 * np.sin(ang_in_rad) + y0 * np.cos(ang_in_rad) + s1[0] / 2
    return np.array((xx, yy))


def align_face(img, d, norm=True):
    """align face as to have eye horizontal and scale to have 48 px between eye center and mouth center

        Note:


        Args:
            img: image
            d: dlib detection
            norm: face normalisation

        Returns:
           aligned face's image
    """
    predictor_path = os.path.join(os.path.dirname(__file__), 'shape_predictor_68_face_landmarks.dat')
    #     detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(predictor_path)

    # (x, y, x1, y1) = d.left(), d.top(), d.right(), d.bottom()
    # #     print( (x, y, x1, y1))
    #
    # x = int(max(0, x))
    # y = int(max(0, y))
    # y1 = int(min(img.shape[0], y1))
    # x1 = int(min(img.shape[1], x1))

    shape = predictor(img, d)

    NOSE = 30
    RIGHT_EYE = 45
    LEFT_EYE = 36

    left_eye = shape.part(LEFT_EYE)
    right_eye = shape.part(RIGHT_EYE)

    align_angle = get_angle_with_X(left_eye, right_eye)
    nose = shape.part(NOSE)

    #     print(left_eye)
    #     print(right_eye)
    #     print(align_angle)


    if norm:
        MOUTH_RIGHT = 48
        MOUTH_LEFT = 54

        mouth_right = shape.part(MOUTH_RIGHT)
        mouth_left = shape.part(MOUTH_LEFT)

        mouth_center = center_between_points(mouth_left, mouth_right)
        eye_center = center_between_points(left_eye, right_eye)

        ec_mc_y = 48
        ec_y = 40
        crop_size = 128

        scale = ec_mc_y * 1. / np.linalg.norm(mouth_center - eye_center)

        old_size = img.shape[:2]

        #         img = rotateImage(img, (nose.x, nose.y), -align_angle)
        img = rotateImage(img, (old_size[1] / 2, old_size[0] / 2), -align_angle)

        new_size = img.shape[:2]

        nose_cropped = transform_points((nose.x, nose.y),
                                        align_angle,
                                        old_size,
                                        new_size)

        eye_center_cropped = transform_points(eye_center,
                                              align_angle,
                                              old_size,
                                              new_size)
        mouth_center_cropped = transform_points(mouth_center,
                                                align_angle,
                                                old_size,
                                                new_size)

        img_resized = cv2.resize(img, (0, 0), fx=scale, fy=scale)

        img_center = np.array(img.shape[:2][::-1]) / 2
        resized_center = np.array(img_resized.shape[:2][::-1]) / 2

        #         print(eye_center_cropped)
        #         print(img_center)
        #         print(resized_center)

        eye_center_cropped = (eye_center_cropped - img_center) * scale + resized_center
        eye_center_cropped = np.array(eye_center_cropped, dtype=np.int)

        #         print("after scale %f" % (scale))
        #         print(eye_center_cropped)


        img = img_resized

        h, w = img.shape[:2]
        x = max(eye_center_cropped[0] - crop_size / 2, 0)
        x1 = min(x + crop_size, w)
        y = max(eye_center_cropped[1] - ec_y, 0)
        y1 = min(y + crop_size, h)

        return img[y:y1, x:x1]
    # cv2.circle(img, tuple(eye_center_cropped), 2, (0, 255, 0))
    #         cv2.circle(img, tuple(np.array(mouth_center_cropped, np.int)), 2, (0, 0, 255))
    #         cv2.circle(img, tuple(np.array(nose_cropped, np.int)), 2, (0, 0, 255))
    #         return img



    return rotateImage(img, (nose.x, nose.y), -align_angle)


# In[ ]:

def most_sharp(faces, n=10):
    """get ``n`` sharpest images from faces

        Note:


        Args:
            faces: images of faces
            n : number of images to return

        Returns:
           list of at most n sharpest faces
    """
    metrics = []
    for face in faces:
        laplacian = cv2.Laplacian(cv2.cvtColor(face, cv2.COLOR_RGB2GRAY), cv2.CV_64F)
        arr = np.sort(np.ravel(laplacian))
        metric = arr[arr > 0].sum()
        metrics.append(metric)

    idx = np.argsort(metrics)

    return [faces[x] for x in idx[:n]]


# In[ ]:
