# coding: utf-8

# In[ ]:

import cv2
import numpy as np
import os
import copy
import dlib
from multiprocessing import Pool
import time
import requests
import cStringIO
import base64
import logging
import logging.handlers

from face_detector import Detector
from videostream import *
from helpers import *

# %load_ext autoreload
# %autoreload 2


# In[ ]:

## api methods
def get_token(ip=None):
    """get auth token from recognition server

        Note:

        Args:
            ip: ip address of the server, if None get from config

        Returns:
            None if successful, token_ otherwise.

    """
    logger = logging.getLogger()  # get_multiprocess_logger()
    config = get_config()
    section = 'auth'
    username = config.get(section, 'username')
    password = config.get(section, 'password')
    lang = config.get(section, 'lang')

    if ip is None:
        ip = config.get('general', 'server')

    data = {'username': username, 'password': password, 'lang': lang}
    rp = requests.post(url='https://{}/api-token-auth/'.format(ip), data=data, verify=False)

    # logging = logging.getlogging()
    if rp.status_code != 200:
        logger.error("Can not get auth token from server. Error code: {}".format(rp.status_code))
        return None

    logger.info("Get auth token. Status Code: ".format(rp.status_code))
    #   print rp.json()
    token_ = rp.json()['token']
    return token_


def send_visit(photos, token, ip=None):
    """send visit to api

        Note:


        Args:
            photos: list of photos
            token: server's auth token
            ip:    server's ip, if None get from config

        Returns:
            rp_visit: resopnse from server

    """
    if ip is None:
        config = get_config()
        ip = config.get('general', 'server')
    files = {}
    for i, f in enumerate(photos):
        #         print i
        #         print i, f
        #         output = cStringIO.StringIO()
        #         output.write(f)
        fname = 'tmp{}.jpg'.format(i)

        cv2.imwrite(fname, f)
        files['photo' + str(i)] = open(fname, 'r')

    visit_data = {'create_person': True}
    rp_visit = requests.post(url='https://{}/visitmulti/'.format(ip),
                             headers={'Authorization': 'token ' + token},
                             data=visit_data, files=files, verify=False)
    logger.info(rp_visit.content)
    return rp_visit

def verify_visit(faces, uid):
    """verify detections for faces and send visit to api

        Note:


        Args:
            faces: images of potential faces
            uid : uid of identity, only used for debugging puposes

        Returns:
           response from server
    """
    dir_name = uid.hex
    resp = None
    faces = most_sharp(faces[:20])
    best_faces = choose_faces_for_visit(faces)
    root = './visits'
    if not os.path.exists(root):
        os.mkdir(root)
    os.mkdir(os.path.join(root, dir_name))
    for i, face in enumerate(faces):
        cv2.imwrite(os.path.join(root, dir_name, '{}.jpg'.format(i)), face)
    if len(best_faces) > 0:
        resp = send_visit(best_faces, get_token())

    return resp


def verify_callback(resp):
    """Check response and log"""
    logger = logging.getLogger()  # get_multiprocess_logger()

    if resp is None:
        logger.error("Error while sending visit. Visit was not verified")
        return

    if resp.status_code == 201:
        logger.info("Send visit. Visit id: {}".format(resp.json()['id']))
    else:
        logger.error("Error while sending visit. Error code: {}".format(resp.status_code))


def choose_faces_for_visit(faces, number=5, align=True):
    """check detections for faces and align
       prefer typeface 0, 4, 5 - frontal, frontal left rotate, frontal right rotate
       bigger the score better the face

    """
    detector = dlib.get_frontal_face_detector()

    # make array [face_id, score, face_type]
    face_scores = []
    aligned_faces = []

    for i, face in enumerate(faces):
        dets, scores, idx = detector.run(face, 1)

        for j, d in enumerate(dets):
            #             print("Detection {}, score: {}, face_type:{}".format(
            #                 d, scores[j], idx[j]))
            face_scores += [[i, scores[j], idx[j]]]
            aligned_faces += [align_face(face, d)]
            break
        if len(dets) == 0:
            aligned_faces += [[]]

    if len(face_scores) == 0:
        return []

    def face_type(x):
        """prefer frontal faces"""
        if x > 4:
            return 0.5
        return x

    face_scores = np.array(sorted(face_scores, key=lambda x: (face_type(x[2]), -x[1]), reverse=False))[:, 0]
    # print(face_scores)

    idxs = np.array(face_scores[:number], dtype=int)
    if align:
        return [aligned_faces[i] for i in idxs]
    return [faces[i] for i in idxs]


def get_config(path=os.path.join(os.path.dirname(__file__), 'stream_handler_config.ini')):
    userlist_config = ConfigParser.SafeConfigParser()
    userlist_config.readfp(open(path))
    return userlist_config


def read_stream_url_from_config():
    config = get_config()
    section = 'camera'

    user = config.get(section, 'user')
    password = config.get(section, 'password')
    ip = config.get(section, 'ip')
    port = config.get(section, 'port')
    channel = config.get(section, 'channel')

    return 'rtsp://{user}:{password}@{ip}:{port}/{channel}'.format(user=user,
                                                                   password=password,
                                                                   ip=ip,
                                                                   port=port,
                                                                   channel=channel)


if __name__ == '__main__':

    import ConfigParser
    import sys
    import argparse

    parser = argparse.ArgumentParser(description='arg pars')
    parser.add_argument('--video', dest='videofile', default=None,
                        help='video path')
    parser.add_argument('--log', dest='loglevel', default='info',
                        help='logging level')
    args = parser.parse_args()

    config = get_config()
    section = 'general'
    CAMERA_TIMEOUT = int(config.get(section, 'timeout'))
    pool = Pool(int(config.get(section, 'pool_size')))
    scale = float(config.get(section, 'scale_frame'))

    logger = logging.getLogger()
    loglevel = args.loglevel
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    logger.setLevel(numeric_level)

    logger.info(args)

    if args.videofile is None:
        stream_url = read_stream_url_from_config()
    else:
        stream_url = args.videofile

    detector = Detector()
    stream = True
    try:
        logging.info('Start working ...')
        while (stream):

            #             camera = cv2.VideoCapture(stream_url)
            camera = WebcamVideoStream(src=stream_url).start()

            if not camera.isOpened():
                logging.error('Can not open camera at {}'.format(stream_url))
                logging.info('Retry connection after timeout: {} seconds'.format(CAMERA_TIMEOUT))
                time.sleep(CAMERA_TIMEOUT)
                continue

            "key: [faces]"
            visits = {}
            "key: bool"
            visit_is_sent = {}
            MIN_FACE_TO_SEND = 20

            start = time.time()
            n_frames = 0

            ret, frame = camera.read()

            while ret:
                frame = cv2.resize(frame, (0, 0), fx=scale, fy=scale)
                dets = detector.get_detected(frame)

                for k, v in dets.items():

                    x, y, x1, y1 = np.array([v.left(), v.top(), v.right(), v.bottom()], dtype=int)
                    face = np.array(frame[y:y1, x:x1])
                    if k in visits:
                        visits[k] += [face]
                    else:
                        visits[k] = [face]

                for k, v in visits.items():
                    if k in dets:
                        if len(visits[k]) >= 20:
                            if k not in visit_is_sent:
                                pool.apply_async(verify_visit, args=(v, k), callback=verify_callback)
                                visit_is_sent[k] = True
                        continue
                    else:
                        if k not in visit_is_sent:
                            ## send to pool
                            pool.apply_async(verify_visit, args=(v, k), callback=verify_callback)
                        #                 verify_visit(v, k)
                        visits.pop(k, None)
                        visit_is_sent.pop(k, None)

                n_frames += 1
                t = time.time() - start
                h, w = frame.shape[:2]

                if n_frames % 10 == 0:
                    logging.info('fps: {:2.2f}, resolution: {}x{}'.format((n_frames * 1. / t), w, h))

                if numeric_level == logging.DEBUG:
                    copy_frame = copy.deepcopy(frame)
                    for k, v in dets.items():
                        x, y, x1, y1 = np.array([v.left(), v.top(), v.right(), v.bottom()], dtype=int)
                        cv2.putText(copy_frame, k.hex, (x, y - 20), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0))
                        cv2.rectangle(copy_frame, (x, y), (x1, y1), (0, 255, 0))

                    cv2.putText(copy_frame, '%2.1f fps' % (n_frames * 1. / t), (w - 200, h - 100),
                                cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0))
                    cv2.putText(copy_frame, '{}x{}'.format(w, h), (w - 200, h - 50), cv2.FONT_HERSHEY_COMPLEX, 1,
                                (0, 255, 0))

                    #                     logging.debug('fps: {:2.2f}, resolution: {}x{}'.format((n_frames*1. / t), w, h))
                    cv2.imshow('camera', copy_frame)
                    cv2.waitKey(1)

                ret, frame = camera.read()
                if n_frames > 200:
                    n_frames = 0
                    start = time.time()

            logging.info('Can not read frames')
            logging.info('Release camera: {}'.format(stream_url))
            camera.release()
            cv2.destroyAllWindows()

            logging.info('Retry connection after timeout: {} seconds'.format(CAMERA_TIMEOUT))
            time.sleep(CAMERA_TIMEOUT)

    except:
        pass

    logging.info('release camera')
    camera.release()
    logging.info('close pool')
    pool.close()
    logging.info('joining pool')
    pool.join()
    logging.info('exiting')  # time.sleep(2)
    #         raise
